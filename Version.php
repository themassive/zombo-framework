<?php
/**
 * Class to store and retrieve the version of framework
 *
 * @category   Zombo
 * @package    Zombo_Version
 * @copyright  Copyright © 2012 TheMassiVe (http://www.themassive.ru)
 * @license    http://www.apache.org/licenses/LICENSE-2.0    Apache License, Version 2.0
 */
final class Zombo_Version
{
  /**
   * Current framework version
   */
  const VERSION = '0.01.001.0001';

  /**
   * The latest version of framework available. If connection to API can't be established, 'n/a' stored as version
   *
   * @var <String>
   */
  protected static $_latest;

  /**
   * Fetches the version of the latest stable release
   *
   * @link http://dev.themassive.ru/zombo/download/latest
   * @return <String>
   */
  public static function getLatestVersion()
  {
    if(!self::$_latest)
    {
      self::$_latest = 'n/a';

      $handle = @fopen('http://dev.themassive.ru/zombo/api/version/stable/latest', 'r');

      if(false !== $handle)
      {
        self::$_latest = stream_get_contents($handle);
        fclose($handle);
      }
    }

    return self::$_latest;
  }
}